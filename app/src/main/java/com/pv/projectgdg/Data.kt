package com.pv.projectgdg

import android.content.Context
import com.google.gson.annotations.SerializedName
import java.io.InputStream
import java.nio.charset.Charset
import java.util.*

class Mock {
    companion object {
        fun getFreeData() : String = "[{id:bitcoin,name:Bitcoin,symbol:BTC,rank:1,price_usd:573.137,price_btc:1.0,24h_volume_usd:72855700.0,market_cap_usd:9080883500.0,available_supply:15844176.0,total_supply:15844176.0,percent_change_1h:0.04,percent_change_24h:-0.3,percent_change_7d:-0.57,last_updated:1472762067},{id:ethereum,name:Ethereum,symbol:ETH,rank:2,price_usd:12.1844,price_btc:0.021262,24h_volume_usd:24085900.0,market_cap_usd:1018098455.0,available_supply:83557537.0,total_supply:83557537.0,percent_change_1h:-0.58,percent_change_24h:6.34,percent_change_7d:8.59,last_updated:1472762062}]"

        fun loadJsonFile(context: Context) : String {
            var jsonString: String
            var inputStream: InputStream = context.assets.open("mockData.json")
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            jsonString = String(buffer, Charset.defaultCharset())
            return jsonString
        }
    }
}

class Colors {
    companion object {
        fun getRandomColor() : String {
            val a: Array<String> = arrayOf("#f44336", "#673AB7", "#FF9800", "#607D8B")
            return a[Random().nextInt(4)]
        }
    }
}

data class CryptoItem (@SerializedName("id")var id: String,
                       @SerializedName("name")var name: String,
                       @SerializedName("symbol")var symbol: String,
                       @SerializedName("rank")var rank: String,
                       @SerializedName("price_usd")var price_usd: String,
                       @SerializedName("price_btc")var price_btc: String,
                       @SerializedName("24h_volume_usd")var h_volume_usd: String,
                       @SerializedName("market_cap_ust")var market_cap_usd: Double,
                       @SerializedName("available_supply")var available_supply: Double,
                       @SerializedName("total_supply")var total_supply: Double,
                       @SerializedName("percent_change_1h")var percent_change_1h: Double,
                       @SerializedName("percent_change_24h")var percent_change_24h: Double,
                       @SerializedName("percent_change_7d")var percent_change_7d: Double,
                       @SerializedName("last_updated")var last_updated: Double)