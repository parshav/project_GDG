package com.pv.projectgdg

import android.graphics.Color
import android.support.annotation.LayoutRes
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class CryptoItemAdapter(val data: Array<CryptoItem>) : RecyclerView.Adapter<CryptoItemAdapter.ViewHolder>() {

    var useColors: Boolean = BuildConfig.showColors

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        holder?.itemTitle?.text = data[position].name
        holder?.itemPrice?.text = data[position].price_usd
        holder?.itemCode?.text = data[position].symbol

        if (useColors) {
            holder?.itemCard?.setBackgroundColor(Color.parseColor(Colors.getRandomColor()))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = parent?.inflate(R.layout.item_currency, false)
        return ViewHolder(v!!)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemTitle = itemView.findViewById<TextView>(R.id.itemTitle)
        var itemPrice = itemView.findViewById<TextView>(R.id.itemPrice)
        var itemCode = itemView.findViewById<TextView>(R.id.itemCode)
        var itemCard = itemView.findViewById<View>(R.id.itemCard)
    }

    fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }
}